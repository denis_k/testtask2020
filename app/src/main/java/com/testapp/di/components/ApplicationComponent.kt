package com.testapp.di.components

import com.testapp.di.modules.ApplicationModule
import com.testapp.di.modules.NetworkModule
import com.testapp.network.DataRemoteDataSource
import com.testapp.ui.main.MainActivity
import com.testapp.ui.search.SearchActivity
import dagger.Component
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class])
interface ApplicationComponent {

    fun inject(activity: MainActivity)
    fun inject(activity: SearchActivity)

}

class DataRepository @Inject constructor(
    val localDataSource: DataLocalDataSource,
    val remoteDataSource: DataRemoteDataSource
)

class DataLocalDataSource @Inject constructor()
