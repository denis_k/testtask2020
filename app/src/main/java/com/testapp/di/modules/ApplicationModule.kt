package com.testapp.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: Application) {

    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return app
    }

}
