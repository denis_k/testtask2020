package com.testapp.di.modules

import com.testapp.network.TheMoviedbApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    private object HOLDER {
        val INSTANCE: TheMoviedbApi =
            TheMoviedbApi.create()
    }

    companion object {
        val instance: TheMoviedbApi by lazy { HOLDER.INSTANCE }
    }

    @Singleton
    @Provides
    fun provideTheMoviedbApi(): TheMoviedbApi {
        return HOLDER.INSTANCE
    }
}