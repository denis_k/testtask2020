package com.testapp.ui.search

import android.text.TextUtils
import androidx.appcompat.widget.SearchView
import com.testapp.di.components.DataRepository
import com.testapp.di.modules.NetworkModule
import com.testapp.models.MovieResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchViewModel @Inject constructor(private var data: DataRepository) {

    var view: SearchViewInterface? = null
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val observer: DisposableObserver<MovieResponse>
        get() = object : DisposableObserver<MovieResponse>() {

            override fun onNext(@NonNull MovieResponse: MovieResponse) {
                view?.hideProgressBar()
                view?.displayResult(MovieResponse)
            }

            override fun onError(@NonNull e: Throwable) {
                e.printStackTrace()
                view?.hideProgressBar()
                view?.displayError("Нет соединения")
            }

            override fun onComplete() {
                view?.hideProgressBar()
            }
        }

    private var searchQuery : String = ""

    private fun getObservableQuery(searchView: SearchView): Observable<String> {

        val publishSubject = PublishSubject.create<String>()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(text: String): Boolean {
                searchQuery = text
                publishSubject.onNext(text)
                return true
            }

            override fun onQueryTextChange(text: String): Boolean {
                searchQuery = text
                publishSubject.onNext(text)
                return true
            }
        })

        return publishSubject
    }

    fun subscribe(searchView: SearchView) {

        val disposable = getObservableQuery(searchView)
            .filter { s -> !TextUtils.isEmpty(s) }
            .debounce(2, TimeUnit.SECONDS)
            .distinctUntilChanged()
            .switchMap<MovieResponse> { s ->
                data.remoteDataSource.searchMovies(s)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith<DisposableObserver<MovieResponse>>(observer)
        compositeDisposable.add(disposable)
    }

    fun getMovies(page: Int = 1) {
        if (!TextUtils.isEmpty(searchQuery)) {
            val disposable = data.remoteDataSource
                .searchMovies(searchQuery, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith<DisposableObserver<MovieResponse>>(observer)
            compositeDisposable.add(disposable)
        }
    }

    fun stop() {
        compositeDisposable.clear()
    }

    fun destroy() {
        compositeDisposable.dispose()
    }
}
