package com.testapp.ui.search;

import com.testapp.models.MovieResponse

interface SearchViewInterface {

    fun showToast(str: String)
    fun displayResult(movieResponse: MovieResponse)
    fun displayError(text: String)
    fun showProgressBar()
    fun hideProgressBar()
}
