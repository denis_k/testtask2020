package com.testapp.ui.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.testapp.App
import com.testapp.R
import com.testapp.adapters.EndlessRecyclerViewScrollListener
import com.testapp.adapters.MoviesAdapter
import com.testapp.models.MovieResponse
import com.testapp.ui.main.MainViewModel
import javax.inject.Inject

class SearchActivity : AppCompatActivity(), SearchViewInterface {

    @Inject
    lateinit var viewModel: SearchViewModel

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var searchView: SearchView

    private lateinit var scrollListener: EndlessRecyclerViewScrollListener
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        (applicationContext as App).appComponent.inject(this)

        setupViews()
    }

    private fun setupViews() {
        viewModel.view = this

        val list : RecyclerView = findViewById(R.id.list)
        adapter = MoviesAdapter(this)
        list.adapter = adapter;
        scrollListener =
            object : EndlessRecyclerViewScrollListener(list.layoutManager as LinearLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    viewModel.getMovies(page+1)
                }
            }
        list.addOnScrollListener(scrollListener)

        swipeRefresh = findViewById(R.id.swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            scrollListener.resetState()
            viewModel.getMovies()
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.stop()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu_search, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(
            searchManager
                .getSearchableInfo(componentName)
        )
        searchView.queryHint = "Enter Movie name.."

        viewModel.subscribe(searchView)


        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        return if (id == R.id.action_search) {
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun showToast(str: String) {
        Toast.makeText(this@SearchActivity, str, Toast.LENGTH_LONG).show()
    }

    override fun displayResult(movieResponse: MovieResponse) {
        movieResponse.results.let {
            if (movieResponse.page == 1) {
                adapter.movieList.clear()
                adapter.notifyDataSetChanged()
            }
            adapter.movieList.addAll(it)
            adapter.notifyDataSetChanged()
        }
    }

    override fun displayError(text: String) {
        showToast(text)
    }

    override fun showProgressBar() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideProgressBar() {
        swipeRefresh.isRefreshing = false
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.destroy()
    }
}
