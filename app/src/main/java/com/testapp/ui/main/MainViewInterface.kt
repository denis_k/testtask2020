package com.testapp.ui.main

import com.testapp.models.MovieResponse

interface MainViewInterface {

    fun showToast(s: String)
    fun showProgressBar()
    fun hideProgressBar()
    fun displayMovies(movieResponse: MovieResponse)
    fun displayError(text: String)
}