package com.testapp.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.testapp.App
import com.testapp.R
import com.testapp.adapters.EndlessRecyclerViewScrollListener
import com.testapp.adapters.MoviesAdapter
import com.testapp.models.MovieResponse
import com.testapp.ui.search.SearchActivity
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainViewInterface {

    @Inject
    lateinit var viewModel: MainViewModel

    private lateinit var toolbar: Toolbar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    private lateinit var adapter: MoviesAdapter
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (applicationContext as App).appComponent.inject(this)

        setupViews()
        viewModel.getMovies()
    }

    private fun setupViews() {
        viewModel.view = this

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val list : RecyclerView = findViewById(R.id.list)
        adapter = MoviesAdapter(this)
        list.adapter = adapter;
        scrollListener =
            object : EndlessRecyclerViewScrollListener(list.layoutManager as LinearLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    viewModel.getMovies(page + 1)
                }
            }
        list.addOnScrollListener(scrollListener)

        swipeRefresh = findViewById(R.id.swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            adapter.movieList.clear()
            adapter.notifyDataSetChanged()
            scrollListener.resetState()
            viewModel.getMovies()
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.stop()
    }

    override fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun showProgressBar() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideProgressBar() {
        swipeRefresh.isRefreshing = false
    }

    override fun displayMovies(movieResponse: MovieResponse) {
        movieResponse.results.let {
            adapter.movieList.addAll(it)
            adapter.notifyDataSetChanged()
        }
    }

    override fun displayError(text: String) {
        showToast(text)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        if (id == R.id.search) {
            val i = Intent(this@MainActivity, SearchActivity::class.java)
            startActivity(i)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.destroy()
    }
}