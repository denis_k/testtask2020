package com.testapp.ui.main

import com.testapp.di.components.DataRepository
import com.testapp.models.MovieResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(private var data: DataRepository) {

    var view: MainViewInterface? = null
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val observer: DisposableObserver<MovieResponse>
        get() = object : DisposableObserver<MovieResponse>() {

            override fun onNext(@NonNull movieResponse: MovieResponse) {
                view?.hideProgressBar()
                view?.displayMovies(movieResponse)
            }

            override fun onError(@NonNull e: Throwable) {
                e.printStackTrace()
                view?.hideProgressBar()
                view?.displayError("Нет соединения")
            }

            override fun onComplete() {
                view?.hideProgressBar()
            }
        }

    fun getMovies(page: Int = 1) {
        if (page == 1) {
            view?.showProgressBar()
        }

        val disposable = data.remoteDataSource
            .getMovies(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith<DisposableObserver<MovieResponse>>(observer)
        compositeDisposable.add(disposable)
    }

    fun stop() {
        compositeDisposable.clear()
    }

    fun destroy() {
        compositeDisposable.dispose()
    }
}
