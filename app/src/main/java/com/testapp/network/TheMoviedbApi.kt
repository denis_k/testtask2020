package com.testapp.network

import com.testapp.models.MovieResponse
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface TheMoviedbApi {

    @GET("movie/popular")
    fun getMovies(
        @Query("api_key") api_key: String = API_KEY,
        @Query("page") page: Int
    ): Single<MovieResponse>

    @GET("search/movie")
    fun getMoviesBasedOnQuery(
        @Query("api_key") api_key: String = API_KEY,
        @Query("query") q: String,
        @Query("page") page: Int
    ): Single<MovieResponse>

    companion object {
        private const val API_KEY = "5108cb5fa6f97ecfbb8b0b3892f536c6"
        private const val BASE_URL = "http://api.themoviedb.org/3/"
        fun create(): TheMoviedbApi {

            val client = OkHttpClient.Builder()
                .build()
            return Retrofit.Builder()
                .baseUrl(HttpUrl.parse(BASE_URL)!!)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TheMoviedbApi::class.java)
        }
    }
}