package com.testapp.network;

import android.content.Context
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.testapp.models.MovieResponse
import io.reactivex.Observable
import javax.inject.Inject

class DataRemoteDataSource @Inject constructor(private val context: Context, private val rest: TheMoviedbApi) {


    fun getMovies(page: Int = 1): Observable<MovieResponse> {
        return ReactiveNetwork
            .observeNetworkConnectivity(context)
            .flatMapSingle { rest.getMovies(page = page)}
    }

    fun searchMovies(q: String, page: Int = 1): Observable<MovieResponse> {
        return ReactiveNetwork
            .observeNetworkConnectivity(context)
            .flatMapSingle { rest.getMoviesBasedOnQuery(q = q, page = page)}
    }
}