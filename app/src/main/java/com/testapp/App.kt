package com.testapp


import android.app.Application
import com.testapp.di.components.DaggerApplicationComponent
import com.testapp.di.modules.ApplicationModule


class App : Application() {

    val appComponent = DaggerApplicationComponent
        .builder()
        .applicationModule(ApplicationModule(this))
        .build()

    override fun onCreate() {
        super.onCreate()
    }

}