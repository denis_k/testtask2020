package com.testapp.adapters

import android.content.Context
import android.os.Build
import android.transition.Slide
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.widget.PopupWindowCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.bumptech.glide.Glide
import com.testapp.R
import com.testapp.models.Movie


class MoviesAdapter(private var context: Context, var movieList: MutableList<Movie> = ArrayList()) :
    RecyclerView.Adapter<MoviesAdapter.MoviesHolder>() {

    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var overviewFullY = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        46f,
        context.resources.displayMetrics
    ).toInt()

    private val popupWindow : PopupWindow

    init {
        val view = inflater.inflate(R.layout.popup_text, null)
        popupWindow = PopupWindow(
            view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesHolder {
        val v = inflater.inflate(R.layout.row_movies, parent, false)
        return MoviesHolder(v)
    }

    override fun onBindViewHolder(holder: MoviesHolder, position: Int) {

        holder.tvTitle.text = movieList[position].title
        holder.tvOverview.text = movieList[position].overview
        holder.tvReleaseDate.text = movieList[position].releaseDate
        Glide.with(context)
            .load("https://image.tmdb.org/t/p/w500/" + movieList[position].posterPath)
            .into(holder.ivMovie)

        holder.tvOverview.setOnClickListener {
            showOverviewFull(holder.itemView as ViewGroup, movieList[position].overview)
        }

        popupWindow.dismiss()
    }

    private fun showOverviewFull(rootView: ViewGroup, text: String?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.RIGHT
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut
        }

        popupWindow.contentView.findViewById<TextView>(R.id.text).text = text
        popupWindow.contentView.findViewById<ImageView>(R.id.close).setOnClickListener {
            popupWindow.dismiss()
        }

        TransitionManager.beginDelayedTransition(rootView)
        PopupWindowCompat.showAsDropDown(
            popupWindow,
            rootView,
            0,
            -overviewFullY,
            Gravity.CENTER
        )
    }

    inner class MoviesHolder(v: View) : RecyclerView.ViewHolder(v) {

        internal var tvTitle: TextView = v.findViewById(R.id.tvTitle)
        internal var tvOverview: TextView = v.findViewById(R.id.tvOverView)
        internal var tvReleaseDate: TextView = v.findViewById(R.id.tvReleaseDate)
        internal var ivMovie: ImageView = v.findViewById(R.id.ivMovie)
    }
}